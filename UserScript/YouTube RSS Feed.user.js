// ==UserScript==
// @name         Youtube RSS Feed
// @namespace    https://gitea.com/Azumyn/YouTube_Get_RSS_Feed
// @homepage     https://gitea.com/Azumyn/YouTube_Get_RSS_Feed
// @version      0.0.7
// @description  Get the RSS feed of a channel
// @author       A'zumyn
// @match        *://*.youtube.com/*
// @exclude      *://music.youtube.com/*
// @exclude      *://*.music.youtube.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=YouTube.com
// @downloadURL  https://gitea.com/Azumyn/YouTube_Get_RSS_Feed/raw/branch/main/UserScript/YouTube%20RSS%20Feed.user.js
// @updateURL    https://gitea.com/Azumyn/YouTube_Get_RSS_Feed/raw/branch/main/UserScript/YouTube%20RSS%20Feed.user.js
// @grant        GM.xmlHttpRequest
// @connect      youtube.com
// @run-at       document-end
// ==/UserScript==

(function() {
  var ytplayer;
  var url = (document.querySelector('link[type="application/rss+xml"]') || '').href;
  if (url !== undefined) {
    location.href = console.log('[YouTube RSS Feed] rss feed url of the channel is:\n' + url);
  }
  try {
    var channelId = ytplayer.config.args.ucid;
    var rssfeed = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + channelId;
    console.log('[YouTube RSS Feed] rss feed url of the channel is:\n' + rssfeed);
  }
  catch (TypeError) {
    console.error('[YouTube RSS Feed] An unexpected error occurred.');
  }
})();